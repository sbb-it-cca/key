package key

import (
	"github.com/rs/zerolog/log"
	"os"
)

func initEnv(envVariable string) string {
	urlPklp := os.Getenv(envVariable)

	if len(urlPklp) == 0 {
		log.Fatal().Msgf("couldn't find env variable %s", envVariable)
	}

	return urlPklp
}

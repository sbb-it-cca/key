package public

import (
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/suite"
	"testing"
)

const (
	UrlSwisspass       = "Urlswisspass"
	UrlHandelsregister = "Urlhandelregister"
)

type SuiteKey struct {
	suite.Suite
	key *publicKeyImpl
}

func TestServiceTestSuite(t *testing.T) {
	suite.Run(t, new(SuiteKey))
}

func (s *SuiteKey) SetupTest() {
	s.key = NewPublicKeyWithParams(UrlSwisspass, UrlHandelsregister)
}

func (s *SuiteKey) shouldGetSwisspassPklpForSwisspassSuffix() {
	url := s.key.decideUrl("me@swisspass.ch")

	assert.Equal(s.T(), UrlSwisspass, url)
}

func (s *SuiteKey) shouldGetHandelsregisterPklpForNonSwisspassSuffix() {
	url := s.key.decideUrl("me_workapp")

	assert.Equal(s.T(), UrlHandelsregister, url)
}
